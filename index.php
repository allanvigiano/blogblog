<!DOCTYPE html>
<html>
    <head>
        <title>BlogBlog</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/w3.css">
        <link rel="stylesheet" href="css/meuCss.css">
    </head>
    <body>
        <header class="w3-container w3-blue-grey">
            <h1>BlogBlog</h1>
        </header>
        <article class="w3-container">
            <p>
                Meu conteúdo, meu, meu conteúdo!
                Meu conteúdo, meu, meu conteúdo!
                Meu conteúdo, meu, meu conteúdo!
                Meu conteúdo, meu, meu conteúdo!
                Meu conteúdo, meu, meu conteúdo!
            </p>
            <div class="w3-card-4" style="width:50%">
                <header class="w3-container w3-blue">
                    <h1>Pôr do sol na lagoa da Pampulha</h1>
                </header>
                <p>
                    Meu conteúdo, meu, meu conteúdo!
                    Meu conteúdo, meu, meu conteúdo!
                    Meu conteúdo, meu, meu conteúdo!
                    Meu conteúdo, meu, meu conteúdo!
                    Meu conteúdo, meu, meu conteúdo!
                </p>
                <div class="w3-container">
                    <div class="w3-image">
                        <img src="images/DSC01269.JPG" alt="Car" style="height: 500px">
                        <div class="w3-title w3-text-white">Por do Sol na Lagora da Pampulha - BH/MG</div>

                    </div>
                </div>
                <div id="notaPampulha" class="w3-card-24 w3-yellow">
                    <p>Se você for em Belo Horizonte, não se esqueça de visitar a Pampulha!</p>
                </div>
            </div>
        </article>
        <footer class="w3-container w3-blue-grey w3-text-white-opacity">
            <h4>Contatos: </h4>
            <ul>allanvigiano@gmail.com</ul>
            <ul>allanvigiano@gmail.com</ul>
            <ul>allanvigiano@gmail.com</ul>
            <ul>allanvigiano@gmail.com</ul>
        </footer>
    </body>
</html>